 <?php
 include"headerppp.php";
 ?>
 <div id="page-wrapper">
  <div class="row">

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Tabel Petugas
          </div>

          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="table-responsive">

              <table id="12rpl3" class="table table-striped table-bordered table-hover">
            <a href="add_petugas.php"><button type="button" class="btn btn-primary">Tambah Data</button></a>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Nama Lengkap</th>
                    <th>Level</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 include '../../koneksi.php';
                 $no=1;
                 $sql="SELECT * from petugas order by id_petugas desc";
                 $select=mysqli_query($koneksi,$sql);
                 while($tampil=mysqli_fetch_array($select)){
                  ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $tampil['username']; ?></td>
                    <td><?php echo $tampil['password']; ?></td>
                    <td><?php echo $tampil['nama_petugas']; ?></td>
                    <td><?php echo $tampil['level']; ?></td>
                    <td><a class="btn btn outline btn-primary fa fa-edit" href="update_petugas.php?id_petugas=<?php echo $tampil['id_petugas']; ?>"></a> 
                      <a class="btn btn outline btn-danger fa fa-trash-o" href="delete_petugas.php?id_petugas=<?php echo $tampil['id_petugas']; ?>"></a>
                    </td>
                  </tr>
                  <?php
                  
                }
                ?>
              </tbody>
            </table>
            <script>
              $(document).ready(function() {
               $('#12rpl3').DataTable();
             } );
           </script>
         </div>
         <!-- /.table-responsive -->
       </div>
       <!-- /.panel-body -->
     </div>
     <!-- /.panel -->
   </div>
   <!-- /.col-lg-6 -->

   <!-- /.col-lg-6 -->
 </div>
 <!-- /.col-lg-12 -->
</div>
