 <?php
 include"header.php";
 ?>
 <div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
             Form Tambah Data Petugas
         </div>
         <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                   <form action="save_petugas.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
                      <div class="form-group">
                        <label>Username</label>
                        <input name="username" type="text" autocomplete="off" maxlength="50" class="form-control" required="" placeholder="Masukan Nama Anda">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input name="password" type="text" autocomplete="off"  class="form-control" required="" placeholder="Masukan Kode Ruang">
                    </div>
                    <div class="form-group">
                        <label>Nama lengkap</label>
                        <input name="nama_petugas" maxlength="50" autocomplete="off" class="form-control" required="" placeholder="Masukan Keterangan">
                    </div>
                    <div class="form-group">
                        <label>Level</label>
                        <select name="level" required="" class="form-control">
                          <option value="" disabled selected>pilih</option>
                          <?php
                          include"../../koneksi.php";
                          $pilih=mysqli_query($koneksi,"SELECT * FROM petugas");
                          while($tampil=mysqli_fetch_array($pilih)){
                            ?>
                            <option value="<?=$tampil['level'];?>"><?=$tampil['level'];?></option>
                            <?php
                        }
                        ?>
                    </select>

                </div>
                <button type="submit" class="btn btn-default">Simpan</button>
            </form>
        </div>
        <!-- /.col-lg-6 (nested) -->

        <!-- /.col-lg-6 (nested) -->
    </div>
    <!-- /.row (nested) -->
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
