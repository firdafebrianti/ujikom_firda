<?php 
    session_start();
include 'token.php';

    // cek apakah yang mengakses halaman ini sudah login
    if($_SESSION['level']==""){
        header("location:../../index.php?pesan=gagal");
    }

    ?>
<?php
include"../../koneksi.php";
?>
<!DOCTYPE html>
<html lang="en" class="cssmenu">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ISP | ujikom FirdaPe</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../css/cssmenu.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body class="cssmenu">

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php" style="color: white">INVENTARIS SARANA PRASARANA</a>
                </div>
                <!-- /.navbar-header -->

               
                <!-- /.navbar-top-links -->
                
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                     <li class="bgg">
                        <ul id="slide-out" class="side-nav " >
                          <li class="bgg">
                          <div class="userView">
                            <div class="background">
                              <img src="../../images/15.jfif" class="bg" style="width: -moz-available;">
                            </div>
                          <a href="#!user"><img class="img-circle" src="../../images/avatar.png"></a>
                          <p align="center"><?php echo $_SESSION['username']; ?></p>
                        </div>
                        </li>

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-user fa-fw"></i> Master Data<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li><a href="inventaris.php"><i class="fa fa-file-text fa-fw"></i>Inventaris</a></li>
                                    <li><a href="peminjam.php"><i class="fa fa-file-text fa-fw"></i>Peminjam</a></li>
                                    <li><a href="pengembalian.php"><i class="fa fa-file-text fa-fw"></i>Pengembalian</a></li>
                                    <li><a href="ruang.php"><i class="fa fa-file-text fa-fw"></i>Ruang</a></li>
                                    <li><a href="petugas.php"><i class="fa fa-file-text fa-fw"></i>Petugas</a></li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li><a href="#"><i class="fa fa-file fa-fw"></i>Backup&Restore<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li><a href="backup-data.php"><i class="fa fa-download fa-fw"></i>BackUp</a></li>
                                    <li><a href="restore-data.php"><i class="fa fa-upload fa-fw"></i>Restore</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-download fa-fw"></i>Export<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                   <li><a href="../export/exportpp.php">Excel</a></li>                                     
                                    <li><a href="../export/printpp.php">PDF</a></li>                              
                                </ul>
                            </li>
                            <li>
                                <a href="logout.php"><i class=" fa fa-sign-out fa-fw">Logout</i></a>
                            </li>                                           
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>         
            <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
            <script type="text/javascript" src="../../assets/js/jquery.dataTables.min.js"></script>

            <script>
                $(document).ready(function() {
                 $('#tester').DataTable();
             } );
         </script>
     </div>
     <!-- /.table-responsive -->
 </div>
 <!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-6 -->

<!-- /.col-lg-6 -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
  
    <!-- /.col-lg-6 -->

    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
 
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
       
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<div class="row">
  
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
        
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../../dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

</body>

</html>
