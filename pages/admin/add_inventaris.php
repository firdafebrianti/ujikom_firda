 <?php
 include"header.php";
 ?>
<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
         Form Tambah Inventaris
       </div>
       <div class="panel-body">
        <div class="row">
          <div class="col-lg-12">
            <form action="save_inventaris.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
              <div class="form-group">
                <label>Nama Barang</label>
                <input name="nama" type="text" autocomplete="off" maxlength="50" class="form-control" required="" placeholder="Masukan Nama Barang" >
              </div>
              <div class="form-group">
                <label>Kondisi</label>
                <input name="kondisi" type="text" autocomplete="off" maxlength="11" class="form-control" required="" placeholder="Masukan Kondisi Barang" >
              </div>
              <div class="form-group">
                <label>Keterangan</label>
                <input name="keterangan" maxlength="50" autocomplete="off" class="form-control" required="" placeholder="Masukan Keterangan" >
              </div> 
              <div class="form-group">
                <label>Jumlah Barang</label>
                <input name="jumlah" maxlength="50" type="number" autocomplete="off" class="form-control" required="" placeholder="Masukan Jumlah Barang" >
              </div>    
              <div class="form-group">
                <label>Jenis</label>
                <select name="id_jenis" required="" class="form-control">
                  <option value="" disabled selected>pilih</option>
                  <?php
                  include '../../koneksi.php';
                  $pilih=mysqli_query($koneksi,"SELECT * FROM jenis");
                  while($tampil=mysqli_fetch_array($pilih)){
                    ?>
                    <option value="<?=$tampil['id_jenis'];?>"><?=$tampil['nama_jenis'];?></option>
                    <?php
                  }
                  ?>
                </select>
              </div> 
              <div class="form-group">
                <label>Tanggal Register</label>
                <input class="form-control" type="date" name="tanggal_register"  required=""></input>
              </div>    
              <div class="form-group">
                <label>Ruang</label>
                <select name="id_ruang" required="" class="form-control">
                  <option value="" disabled selected>pilih</option>
                  <?php
                  include"../../koneksi.php";
                  $pilih=mysqli_query($koneksi,"SELECT * FROM ruang");
                  while($tampil=mysqli_fetch_array($pilih)){
                    ?>
                    <option value="<?=$tampil['id_ruang'];?>"><?=$tampil['nama_ruang'];?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="input-field col s12">
                <label for="first_name">Kode :</label>
                <input name="kode_inventaris" class="form-control" type="text"  class="validate" data-length="30" maxlength="30" autocomplete="off" required="" >
              </div>
              <div class="form-group">
                <label>Petugas</label>
                <select name="id_petugas" required="" class="form-control">
                  <option value="" disabled selected>pilih</option>
                  <?php
                  $pilih=mysqli_query($koneksi,"SELECT * FROM petugas");
                  while($tampil=mysqli_fetch_array($pilih)){
                    ?>
                    <option value="<?=$tampil['id_petugas'];?>"><?=$tampil['nama_petugas'];?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <button type="submit" class="btn btn-default">Simpan</button>
            </form>
           
        </div>
        <!-- /.col-lg-6 (nested) -->

        <!-- /.col-lg-6 (nested) -->
      </div>
      <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
  </div>
  <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
