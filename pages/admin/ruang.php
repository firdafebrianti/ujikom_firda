<?php
include"headerr.php";
?>
<div id="page-wrapper">
  <div class="row">

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Tabel Ruang
          </div>

          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="table-responsive">

              <table id="12rpl3" class="table table-striped table-bordered table-hover">
            <a href="add_ruang.php"><button type="button" class="btn btn-primary">Tambah Data</button></a>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama ruang</th>
                    <th>Kode ruang</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 include '../../koneksi.php';
                 $no=1;
                 $pilih="SELECT * from ruang order by id_ruang desc";
                 $select=mysqli_query($koneksi,$pilih);
                 while($tampil=mysqli_fetch_array($select)){
                  ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $tampil['nama_ruang']; ?></td>
                    <td><?php echo $tampil['kode_ruang']; ?></td>
                    <td><?php echo $tampil['keterangan']; ?></td>
                    <td><a class="btn btn outline btn-primary fa fa-edit" href="update_ruang.php?id_ruang=<?php echo $tampil['id_ruang']; ?>"></a> 
                      <a class="btn btn outline btn-danger fa fa-trash-o" href="delete_ruang.php?id_ruang=<?php echo $tampil['id_ruang']; ?>"></a>
                    </td>
                  </tr>
                  <?php

                }
                ?>
              </tbody>
            </table>
            
            <script>
              $(document).ready(function() {
               $('#12rpl3').DataTable();
             } );
           </script>
         </div>
         <!-- /.table-responsive -->
       </div>
       <!-- /.panel-body -->
     </div>
     <!-- /.panel -->
   </div>
   <!-- /.col-lg-6 -->

   <!-- /.col-lg-6 -->
 </div>
 <!-- /.col-lg-12 -->
</div>
