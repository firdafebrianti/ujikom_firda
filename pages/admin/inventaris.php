 <?php
 include"header.php";
 ?>
 <div id="page-wrapper">
  <div class="row">

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Tabel Inventaris
          </div>

          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="table-responsive">

              <table id="12rpl3" class="table table-striped table-bordered table-hover" >
                <thead>
                <a href="add_inventaris.php"><button class="btn btn-success" style="margin-bottom: 10px">Tambah Data</button></a>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Kondisi</th>
                    <th>Keterangan</th>
                    <th>Jumlah</th>
                    <th>jenis</th>
                    <th>Tgl register</th>
                    <th>Nama ruang</th>
                    <th>Kode inventaris</th>
                    <th>Nama petugas</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 include'../../koneksi.php';
                 $no=1;
                 $sql="select * from inventaris i join ruang r on i.id_ruang=r.id_ruang join jenis j on i.id_jenis=j.id_jenis join petugas p on p.id_petugas=i.id_petugas order by id_inventaris desc";
                 $select=mysqli_query($koneksi,$sql);
                 while($tampil=mysqli_fetch_array($select)){
                  ?>
                  <tr>
                    <td><?php echo$no++;?></td> 
                    <td><?php echo$tampil['nama'];?></td>
                    <td><?php echo$tampil['kondisi'];?></td>
                    <td><?php echo$tampil['keterangan'];?></td>
                    <td><?php echo$tampil['jumlah'];?></td>
                    <td><?php echo$tampil['nama_jenis'];?></td>
                    <td><?php echo$tampil['tanggal_register'];?></td>
                    <td><?php echo$tampil['nama_ruang'];?></td>
                    <td><?php echo$tampil['kode_inventaris'];?></td>
                    <td><?php echo$tampil['nama_petugas'];?></td>
                    <td>
                    <a class="btn btn outline btn-primary fa fa-edit" href="update_inventaris.php?id_inventaris=<?php echo $tampil['id_inventaris']; ?>"></a> 
                      <a class="btn btn outline btn-danger fa fa-trash-o" href="delete_inventaris.php?id_inventaris=<?php echo $tampil['id_inventaris']; ?>"></a>
                    </td>

                  </tr>
                  <?php

                }
                ?></tbody>
              </table>
                
              <script>
                $(document).ready(function() {
                 $('#12rpl3').DataTable();
               } );
             </script>
           </div>
           <!-- /.table-responsive -->
         </div>
         <!-- /.panel-body -->
       </div>
       <!-- /.panel -->
     </div>
     <!-- /.col-lg-6 -->

     <!-- /.col-lg-6 -->
   </div>
   <!-- /.col-lg-12 -->
 </div>
