 <?php
 include"header.php";
 ?>
 <div id="page-wrapper">
  <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Form Tambah Ruang
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                 <form action="save_ruang.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
                                      <div class="form-group">
                                            <label>Nama Ruang</label>
                                            <input name="nama_ruang" type="text" autocomplete="off" maxlength="50" class="form-control" required="" placeholder="Masukan Nama Anda">
                                        </div>
                                        <div class="form-group">
                                            <label>Kode Ruang</label>
                                            <input name="kode_ruang" type="text" autocomplete="off" maxlength="11" class="form-control" required="" placeholder="Masukan Kode Ruang">
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <input name="keterangan" maxlength="50" autocomplete="off" class="form-control" required="" placeholder="Masukan Keterangan">
                                        </div>    
                                        <button type="submit" class="btn btn-default">Simpan</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
