 <?php
 include"headerp.php";
 include '../../koneksi.php';
 include 'token.php';
 $id_peminjam=$_GET['id_peminjam'];
 $pilih=mysqli_query($koneksi,"SELECT * from peminjam p join petugas o on p.id_peminjam=o.id_petugas join inventaris i on p.id_inventaris=i.id_inventaris where id_peminjam=$id_peminjam");
 while($tampil = mysqli_fetch_array($pilih)){
  ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
           Form Edit Peminjam
         </div>
         <div class="panel-body" style="margin-left: 23px;">
          <div class="row">
            <div class="col-md-12">
             <form action="" method="post" class="form-horizontal">
              <fieldset>
                <div class="form-group">
                  <div class="form-group col-md-12 ">
                    <label for="typeahead">Nama Peminjam</label>
                    <input type="text" name="nama_petugas" class="form-control" id="typeahead" placeholder="Masukan Nama peminjam" value="<?php echo $tampil['nama_petugas'];?>">
                  </div>
                  <div class="form-group col-md-12">
                    <label for="typeahead">Nama Barang</label>
                    <select name="nama" value="<?=$tampil['nama'];?>" class="form-control">
                      <option value="<?=$tampil['nama'];?>" disabled selected ><?=$tampil['nama'];?></option>

                      <?php
                      include"../../koneksi.php";
                      $pilih1=mysqli_query($koneksi,"SELECT * FROM inventaris");
                      while($tampil1=mysqli_fetch_array($pilih1)){
                        ?>
                        <option value="<?=$tampil1['nama'];?>"><?=$tampil1['nama'];?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>    
                  <div class="form-group col-md-12">
                    <label for="typeahead">Tanggal Pinjam</label>
                    <input type="date" name="tanggal_pinjam" class="form-control" id="typeahead" placeholder="Masukan Tanggal pinjam" value="<?php echo date('Y-m-d', strtotime($qu['tanggal_pinjam'])) ?>">
                  </div>
                  <div class="form-group col-md-12">
                    <label for="typeahead">Status Pinjam</label>
                    <select class="form-control" name="status_peminjam">
                      <option value="pinjam" name="status_peminjam">
                        pinjam
                      </option>
                      <option value="kembali" name="status_peminjam">
                        kembali
                      </option>
                    </select>
                  </div>
                  <br>
                  <button type="submit" class="btn btn-success" name="simpan">Simpan</button>
                  <button type="reset" class="btn btn-danger">Reset</button>
                </fieldset>
              </form>
              <?php
            }
            ?>

          </div>
          <!-- /.col-lg-6 (nested) -->

          <!-- /.col-lg-6 (nested) -->
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<?php
if(isset($_POST['simpan'])){
  $nama_peminjam=$_POST['nama_peminjam'];
  $tanggal_pinjam=$_POST['tanggal_pinjam'];
  $status_peminjam=$_POST['status_peminjam'];
  $id_inventaris=$_POST['id_inventaris'];

  $pilih=mysqli_query($koneksi,"UPDATE peminjam SET nama_peminjam='$nama_peminjam',tanggal_pinjam='$tanggal_pinjam' ,status_peminjam='$status_peminjam' ,id_inventaris='$id_inventaris' WHERE id_peminjam='$id_peminjam'");
  if ($pilih) {
    echo "<script>alert('Data Telah Berhasil Di Edit!');window.location.href='peminjam.php';</script>";
  }else{
    echo "gagal";
  }
}?>