 <?php
 include"headeropp.php";
 ?>
 <div id="page-wrapper">
  <div class="row">

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Tabel Pengembalian
          </div>

          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="table-responsive">

              <table id="12rpl3" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Peminjam</th>
                    <th>Tgl Pinjam</th>
                    <th>Status Peminjaman</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                 <?php
                 include '../../koneksi.php';
                 $no=1;
                 $sql=mysqli_query($koneksi,"SELECT * from peminjam p join petugas o on p.id_peminjam=o.id_petugas join inventaris i on p.id_inventaris=i.id_inventaris join detail_pinjam l on i.id_inventaris=l.id_detail_pinjam");
                 while($tampil=mysqli_fetch_array($sql)){
                  ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $tampil['nama_petugas'];?></td>
                    <td><?php echo $tampil['tanggal_pinjam']; ?></td>
                    <td><?php echo $tampil['status_peminjam']; ?></td>
                    <td><?php echo $tampil['nama'];?></td>
                    <td><?php echo $tampil['jumlah'];?></td>
                    
                  </tr>
                  <?php
                  
                }
                ?>
              </tbody>
            </table>
            <script>
              $(document).ready(function() {
               $('#12rpl3').DataTable();
             } );
           </script>
         </div>
         <!-- /.table-responsive -->
       </div>
       <!-- /.panel-body -->
     </div>
     <!-- /.panel -->
   </div>
   <!-- /.col-lg-6 -->

   <!-- /.col-lg-6 -->
 </div>
 <!-- /.col-lg-12 -->
</div>
