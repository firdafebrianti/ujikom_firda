<?php
include"header.php";
?>
<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
         Form Pinjam Barang
       </div>
       <div class="panel-body">
        <div class="row">
          <div class="col-lg-12">
            <form action="proses_pinjam.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
              <div class="form-group">
                <label>Nama Kamu</label>
                <input name="nama_peminjam" class="form-control" placeholder="Nama kamu ?" required="" lang="50" type="text"></input>
              </div>    
              <div class="form-group">
                <label>Nama Barang</label>
                <select name="nama" required="" class="form-control">
                  <option value="" disabled selected>pilih</option>
                  <?php
                  include '../../koneksi.php';
                  $pilih=mysqli_query($koneksi,"SELECT * FROM inventaris");
                  while($tampil=mysqli_fetch_array($pilih)){
                    ?>
                    <option value="<?=$tampil['id_inventaris'];?>"><?=$tampil['nama'];?></option>
                    <?php
                  }
                  ?>
                </select>
              </div> 
              <div class="form-group">
                <label>Tanggal</label>
                <input type="date" value="<?php echo date('Y-m-d', strtotime($qu['tanggal_pinjam'])) ?>"  name="tanggal_pinjam"
                class="form-control datepicker" required>
              </div>     
              <div class="form-group">
                <label>Pinjam atau kembali ?</label>
                <select name="status_peminjam" required="" class="form-control">
                  <option value="" disabled selected>pilih</option>
                  <?php
                  include '../../koneksi.php';
                  $pilih=mysqli_query($koneksi,"SELECT * FROM peminjam");
                  while($tampil=mysqli_fetch_array($pilih)){
                    ?>
                    <option value="<?=$tampil['status_peminjam'];?>"><?=$tampil['status_peminjam'];?></option>
                    <?php
                  }
                  ?>
                </select>
              </div> 
              <button type="submit" class="btn btn-success">Simpan</button>
            </form>
          </div>
          <!-- /.col-lg-6 (nested) -->

          <!-- /.col-lg-6 (nested) -->
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
