 <?php
 include"header.php";
 ?>
 <style type="text/css">
  .col-md-12{
    margin-top: 20px;
    background-color: lightyellow;
    padding: 20px;
  }.banner{    
    background: #fff;
    display: block;
    padding: 1em;
    border: 1px solid #ebeff6;
    border-color: #ebeff6;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
  }
  .banner h2{
    font-size: 0.9em;
  }.banner h2 a {
    color: #00bcd4;
    text-decoration: none;
    padding: 0.3em;
  }
  .banner h2 i {
    color: #000;
    vertical-align: middle;
    padding: 0.3em;
  }
.asked {
    padding: 1em;
    background: #fff;
    margin: 1em 0 0 0;
    border: 1px solid #ebeff6;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.questions h5 {
    color: #000;
    font-size: 1.3em;
    line-height: 1.6em;
}
.questions p {
    font-size: 0.9em;
    color: #999;
    padding: 1em 0;
    line-height: 2em;
}


</style>
<div id="page-wrapper">
  <div class="row">

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="row">
      <div class="col-lg-12">
        <h2>Frequently Asked Questions</h2>
      </div>
      <div class="banner">
        <h2>
          <a href="dashboard.php">Home</a>
          <i class="fa fa-angle-right"></i>
          <span>Faqs</span>
        </h2>
      </div>     
      <div class="asked">
        <div class="questions">
        <h5>1.What Inventaris sarana dan prasarana?</h5>
          <p>Salah satu aktivitas dalam pengelolaan perlengkapan pendidikan di sekolah  adalah mencatat semua perlengapan yang dimiliki oleh sekolah. Lazimnya, kegiatan pencatatan semua perlengkapan itu disebut dengan istilah inventarisasi perlengkapan pendidikan. Kegiatan tersebut merupakan suatu proses yang berkelanjutan. Secara definitif, inventarisasi adalah pencatatan dan penyusunan daftar barang milik negara secara sistematis, tertib, teratur berdasarkan ketentuan-ketentuan atau pedoman yang berlaku. Inventarisasi sarpras pendidikan adalah kegiatan pencatatan semua sarana prasarana dan merupakan suatu proses berkelanjutan, barang milik negara. Menurut Keputusan Menteri Keuangan RI Nomor Kep. 225/MK/V/4/1971 barang milik negara adalah berupa semua barang yang berasal atau dibeli dengan dana yang bersumber, baik secara keseluruhan atau sebagaian, dari Anggaran Pendapat Belanja Negara (APBN) ataupun dana lainnya yang barang-barangnya di bawah penguasaan pemerintah, baik pusat, provinsi, maupun daerah otonom, baik yang berada di dalam maupun luar negeri.</p>
        </div>
      </div>     
      <div class="asked">
        <div class="questions">
        <h5>2.Landasan Hukum Inventaris sarana dan prasarana?</h5>
          <p>yang mendasari kegiatan inventarisasi perlengkapan sekolah, yaitu

            1. Intruksi Presiden RI Nomor 3 Tahun 1971, tertanggal 30 Maret 1991<br>
            2. Surat Keputusan Menteri Keuangan Nomor Kep. 225/MK/V/4/197, tertanggal 13 April 1971<br>
            3. Instruksi Menteri Pendidikan dan Kebudayaan Nomor 9 Tahun 1971, tertanggal 23 Oktober 1971<br>
            4. Instruksi Menteri Pendidikan dan Kebudayaan Nomor 4/M/1980, tertanggal 24 Mei 1980</p>
        </div>
      </div>     
      <div class="asked">
        <div class="questions">
        <h5>3.Tujuan Inventaris sarana dan prasarana?</h5>
          <p>1. Tercipta ketertiban administrasi barang<br>
              2. Penghematan keuangan<br>
              3. Mempermudah pemeliharaan dan pengawasan barang<br>
              4. Menyediakan data informasi untuk perencanaan</p>
              </p>
        </div>
      </div>     
      <div class="asked">
        <div class="questions">
        <h5>4.Manfaat Inventaris sarana dan prasarana?</h5>
          <p>1.Mencatat dan menghimpun data aset yang dikuasahi unit organisasi/ departemen.<br>
              2.Menyiapkan dan menyediakan bahan laporan pertanggungjawaban atas penguasaan dan pengelolaan aset organisasi/ negara.<br>
              3.Menyiapkan dan menyediakan bahan acuan untuk pengawasan aset organisasi atau negara.<br>
              4.Menyediakan informasi mengenai aset organisasi /negara yang dikuasahi departemen sebagai bahan untuk perencanaan kebutuhan, pengadaan dan pengelolaan perlengkapan departemen.<br>
              5.Menyediakan informasi tentang aset yang dikuasai departemen untuk menunjang perencanaan dan pelaksanaan tugas departemen.</p>
        </div>    
      <div class="asked">
        <div class="questions">
        <h5>4.Cara Menginventarisasi </h5>
          <p>
              Berdasarkan Surat Edaran Menteri Pendidikan dan Kebudayaan RI tanggal 16 Januari 1979 No. 20/MPK/1979, pengurusan barang-barang di sekolah dasar dilakukan oleh kepala sekolah sendiri. Namun, dalam pelaksanaan sehari-hari kepala sekolah sebagai administrator dapat menunjuk stafnya atau guru-guru untuk mengerjakan tugas dan tanggung jawab tersebut (Stoop  & Johnson, 1969). Kegiatan inventarisasi perlengkapan pendidikan meliputi dua kegiatan, yaitu
              <br>
              1)      Kegiatan yang berhubungan dengan pencatatan dan pembuatan kode barang perlengapan
              <br>
              2)      Kegiatan yang berhubungan dengan pembuatan laporan. Berikut ini secara rinci satu persatu.
              <br>
              <b>Pencatatan perlengkapan pendidikan</b>
              Baik barang inventaris maupun barang bukan inventaris  yang diterima sekolah harus dicatat di dalam buku penerimaan. Setelah itu, khusus barang-barang inventaris  dicatat di dalam buku induk inventaris  dan buku golongan inventaris. Sedangkan khusus barang-barang bukan inventaris dicatat di dalam buku induk bukan inventaris dan kartu (bisa juga berupa buku) stok barang.</p>
        </div>
      </div>


    </div>
  </div>
</div>
