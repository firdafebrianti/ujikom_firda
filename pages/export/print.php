<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title> 
   <style>
   </style>
</head>
<body>
<h1 style="text-align: center;">Data Inventaris SMKN 1 CIOMAS</h1>
<table border="1" width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
  <th>No</th>
  <th>Nama</th>
  <th>Kondisi</th>
  <th>Keterangan</th>
  <th>Jumlah</th>
  <th>Jenis</th>
  <th>Tanggal Register</th>
  <th>Nama Ruang</th>
  <th>Kode Inventaris</th>
  <th>Nama Petugas</th>
</tr>
    <?php
		include "../../koneksi.php";
		$no=1;
		$sql="select * from inventaris i join ruang r on i.id_ruang=r.id_ruang join jenis j on i.id_jenis=j.id_jenis join petugas p on p.id_petugas=i.id_petugas";
    $select=mysqli_query($koneksi,$sql);
		while($data=mysqli_fetch_array($select))
		{
		?>
        <tr>
        	<td><?php echo $no++; ?></td>
             <td><?php echo $data['nama']; ?></td>
            <td><?php echo $data['kondisi']; ?></td>
            <td><?php echo $data['keterangan']; ?></td>
             <td><?php echo $data['jumlah']; ?></td>
             <td><?php echo $data['id_jenis']; ?></td>
             <td><?php echo $data['tanggal_register']; ?></td>
             <td><?php echo $data['nama_ruang']; ?></td>
             <td><?php echo $data['kode_inventaris']; ?></td>
             <td><?php echo $data['nama_petugas']; ?></td>
        </tr>
        <?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Inventaris.pdf', 'D');
?>