<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title>
    
   <style>
   table td {word-wrap:break-word;width: 20%;}
   </style>
</head>
<body>
  
<h1 style="text-align: center;">Data Siswa SMKN 1 CIOMAS</h1>
<table border="1" width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
  <th>No</th>
  <th>Username</th>
  <th>Password</th>
  <th>Nama Lengkap</th>
  <th>Id Level</th>
</tr>
    <?php
		include "../../koneksi.php";
		$no=1;
		$sql="select * from petugas";
    $select=mysqli_query($koneksi,$sql);
		while($data=mysqli_fetch_array($select))
		{
		?>
        <tr>
        	<td><?php echo $no++; ?></td>
             <td><?php echo $data['username']; ?></td>
            <td><?php echo $data['password']; ?></td>
            <td><?php echo $data['nama_petugas']; ?></td>
             <td><?php echo $data['level']; ?></td>
        </tr>
        <?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();
        
require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Petugas.pdf', 'D');
?>