<?php ob_start(); ?>
<html>
<head>
  <title>Cetak PDF</title>
  
  <style>
   table td {word-wrap:break-word;width: 20%;}
 </style>
</head>
<body>
  
  <h1 style="text-align: center;">Data Peminjaman</h1>
  <table border="1" width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <th>No</th>
      <th>Nama Peminjam</th>
      <th>Tanggal Kembali</th>
      <th>Status Peminjaman</th>
      <th>Nama Petugas</th>
    </tr>
    <?php
    include '../../koneksi.php';
    $no=1;
    $sql="select * from peminjam p join petugas o on p.id_peminjam=o.id_petugas";
    $select=mysqli_query($koneksi,$sql);
    while($data=mysqli_fetch_array($select))
    {
      ?>
      <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['nama_peminjam'];?></td>
        <td><?php echo $data['tanggal_kembali']; ?></td>
        <td><?php echo $data['status_peminjam']; ?></td>
        <td><?php echo $data['nama_petugas']; ?></td>
      </tr>
      <?php
    }
    ?>
  </table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Pengembalian.pdf', 'D');
?>